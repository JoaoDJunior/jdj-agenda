package com.joaodjunior.jdjagenda.controller;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.joaodjunior.jdjagenda.model.Usuario;
import com.joaodjunior.jdjagenda.service.UsuarioService;

@Controller
@RequestMapping("usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/")
	public ModelAndView buscarUsuario() {
		return new ModelAndView("usuario/login");
	}
	
	@GetMapping("/{email}")
	public ModelAndView buscarUsuario(@PathParam(value="email") String email) {
		return new ModelAndView("home", "usuario", usuarioService.buscarUsuarioPeloEmail(email));
	}
	
	@PostMapping("home")
	public ModelAndView inserirUsuario(@RequestBody Usuario usuario) {
		return new ModelAndView("login", "usuario", usuarioService.inserirUsuario(usuario));
	}
}
