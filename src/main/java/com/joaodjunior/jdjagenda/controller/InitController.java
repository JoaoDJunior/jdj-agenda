package com.joaodjunior.jdjagenda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("jdj")
public class InitController {

	@GetMapping("agenda")
	public ModelAndView init() {
		return new ModelAndView("agenda/home");
	}
}
