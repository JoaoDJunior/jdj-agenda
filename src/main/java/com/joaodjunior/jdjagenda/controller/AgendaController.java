package com.joaodjunior.jdjagenda.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.joaodjunior.jdjagenda.model.Agenda;
import com.joaodjunior.jdjagenda.service.AgendaService;

@Controller
@RequestMapping("agenda")
public class AgendaController {

	@Autowired
	private AgendaService agendaService;
	
	@GetMapping("/")
	public ModelAndView listarAgendas() {
		return new ModelAndView("agenda/home", "agendas", agendaService.listarAgendas());
	}
	
	@GetMapping("/{agendaId}")
	public ModelAndView buscarAgenda(@PathVariable("agendaId") Long agendaId) {
		return new ModelAndView("agenda/view");
	}
	
	@PutMapping("atualizar")
	public ModelAndView atualizarAgenda(@RequestBody Agenda agenda) {
		return new ModelAndView("view", "agenda", agendaService.atualizarAgenda(agenda));
	}
	
	@PostMapping("inserir")
	public ModelAndView inserirAgenda(@RequestBody Agenda agenda) {
		return new ModelAndView("agenda/view", "agenda", agendaService.inserirAgenda(agenda));
	}
	
	@DeleteMapping("remover/{agendaId}")
	public ModelAndView removerAgenda(@PathVariable("agendaId") Long agendaId) {
		agendaService.removerAgenda(agendaId);
		return new ModelAndView("home");
	}
}
