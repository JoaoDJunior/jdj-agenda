package com.joaodjunior.jdjagenda.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.joaodjunior.jdjagenda.model.Contato;
import com.joaodjunior.jdjagenda.service.ContatoService;

@Controller
@RequestMapping("contato")
public class ContatoController {

	@Autowired
	private ContatoService contatoService;
	
	@GetMapping("/listar")
	public List<Contato> listarContatos() {
		return contatoService.listarContatos();
	}
	
	@GetMapping("/{agendaId}")
	public ModelAndView listaContatos(@PathVariable("agendaId") Long agendaId) {
		return new ModelAndView("agenda/view");
	}
	
	@GetMapping("home/{contatoId}")
	public ModelAndView buscarContato(Long contatoId) {
		return new ModelAndView("view", "contato", contatoService.buscarContato(contatoId));
	}
	
	@PutMapping("home/{contatoId}")
	public ModelAndView atualizarContato(@RequestBody Contato contato) {
		return new ModelAndView("view", "contato", contatoService.atualizarContato(contato));
	}
	
	@PostMapping("home")
	public ModelAndView inserirContato(@RequestBody Contato contato) {
		return new ModelAndView("view", "contato", contatoService.inserirContato(contato));
	}
	
	@DeleteMapping("home/{contatoId}")
	public ModelAndView removerContato(@PathParam(value="contatoId") Long contatoId) {
		contatoService.removerContato(contatoId);
		return new ModelAndView("home");
	}
}
