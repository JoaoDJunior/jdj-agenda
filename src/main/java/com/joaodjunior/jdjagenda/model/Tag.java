package com.joaodjunior.jdjagenda.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="CAD_AGENDA_TAG")
public class Tag implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_AGENDA_TAG",nullable=false)
	private Long tagId;
	@Column(name="NO_TAG",nullable=false)
	private String tagNome;
	
	@ManyToOne
	@JoinColumn(name="CAD_AGENDA_ID_AGENDA")
	private Agenda agenda;

	public Tag() {
		super();
	}

	public Tag(Long tagId, String tagNome) {
		super();
		this.tagId = tagId;
		this.tagNome = tagNome;
	}

	public Tag(Long tagId, String tagNome, Agenda agenda) {
		super();
		this.tagId = tagId;
		this.tagNome = tagNome;
		this.agenda = agenda;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getTagNome() {
		return tagNome;
	}

	public void setTagNome(String tagNome) {
		this.tagNome = tagNome;
	}
	
	public Agenda getAgenda() {
		return agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}
	
	
	
}
