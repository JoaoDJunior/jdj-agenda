package com.joaodjunior.jdjagenda.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="CAD_NUMERO")
public class ContatoNumero implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_NUMERO",nullable=false)
	private Long numeroId;
	@Column(name="NU_NUMERO",length=11,nullable=false)
	private String numero;
	
	@ManyToOne
	@JoinColumn(name="CAD_CONTATO_ID_CONTATO")
	private Contato contato;

	public ContatoNumero() {
		super();
	}

	public ContatoNumero(Long numeroId, String numero) {
		super();
		this.numeroId = numeroId;
		this.numero = numero;
	}

	public ContatoNumero(Long numeroId, String numero, Contato contato) {
		super();
		this.numeroId = numeroId;
		this.numero = numero;
		this.contato = contato;
	}

	public Long getNumeroId() {
		return numeroId;
	}

	public void setNumeroId(Long numeroId) {
		this.numeroId = numeroId;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}
	

}
