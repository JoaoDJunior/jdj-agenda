package com.joaodjunior.jdjagenda.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="CAD_USUARIO")
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_USUARIO",nullable=false)
	private Long usuarioId;
	@Column(name="NO_USUARIO",length=60,nullable=false)
	private String nome;
	@Column(name="TX_EMAIL",length=45,nullable=false)
	private String email;
	
	@OneToMany(mappedBy="usuario", cascade=CascadeType.ALL)
	private List<Agenda> agendas;

	public Usuario() {
		super();
	}
	
	public Usuario(Long usuarioId, String nome, String email) {
		super();
		this.usuarioId = usuarioId;
		this.nome = nome;
		this.email = email;
	}

	public Usuario(Long usuarioId, String nome, String email, List<Agenda> agendas) {
		super();
		this.usuarioId = usuarioId;
		this.nome = nome;
		this.email = email;
		this.agendas = agendas;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@JsonIgnore
	public List<Agenda> getAgendas() {
		return agendas;
	}

	public void setAgendas(List<Agenda> agendas) {
		this.agendas = agendas;
	}
	
	
	
}
