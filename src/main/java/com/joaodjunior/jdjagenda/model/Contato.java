package com.joaodjunior.jdjagenda.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="CAD_CONTATO")
public class Contato implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_CONTATO",nullable=false)
	private Long contatoId;
	@Column(name="NO_CONTATO",length=60,nullable=false)
	private String nome;
	@Column(name="TX_EMAIL_CONTATO",length=45,nullable=false)
	private String email;
	
	@OneToMany(mappedBy="contato", cascade=CascadeType.ALL)
	@Column(name="CAD_NUMERO",nullable=false)
	private List<ContatoNumero> numeros;
	
	@ManyToOne
	@JoinColumn(name="CAD_AGENDA_ID_AGENDA")
	private Agenda agenda;

	public Contato() {
		super();
	}

	public Contato(Long contatoId, String nome, String email) {
		super();
		this.contatoId = contatoId;
		this.nome = nome;
		this.email = email;
	}

	public Contato(Long contatoId, String nome, String email, List<ContatoNumero> numeros, Agenda agenda) {
		super();
		this.contatoId = contatoId;
		this.nome = nome;
		this.email = email;
		this.numeros = numeros;
		this.agenda = agenda;
	}

	public Long getContatoId() {
		return contatoId;
	}

	public void setContatoId(Long contatoId) {
		this.contatoId = contatoId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@JsonIgnore
	public List<ContatoNumero> getNumeros() {
		return numeros;
	}

	public void setNumeros(List<ContatoNumero> numeros) {
		this.numeros = numeros;
	}
	
	public Agenda getAgenda() {
		return agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}
	
	
}
