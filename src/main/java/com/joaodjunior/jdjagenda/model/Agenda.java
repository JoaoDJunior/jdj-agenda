package com.joaodjunior.jdjagenda.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="CAD_AGENDA")
public class Agenda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_AGENDA",nullable=false)
	private Long agendaId;
	@Column(name="NO_AGENDA",length=60,nullable=false)
	private String nome;
	@Column(name="DESC_AGENDA",length=400,nullable=false)
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name="CAD_USUARIO_ID_USUARIO")
	private Usuario usuario;
	
	@OneToMany(mappedBy="agenda", cascade=CascadeType.ALL)
	private List<Tag> tags;
	
	@OneToMany(mappedBy="agenda", cascade=CascadeType.ALL)
	private List<Contato> contatos;

	public Agenda() {
		super();
	}

	public Agenda(Long agendaId, String nome, String descricao) {
		super();
		this.agendaId = agendaId;
		this.nome = nome;
		this.descricao = descricao;
	}

	public Agenda(Long agendaId, String nome, String descricao, Usuario usuario, List<Tag> tags,
			List<Contato> contatos) {
		super();
		this.agendaId = agendaId;
		this.nome = nome;
		this.descricao = descricao;
		this.usuario = usuario;
		this.tags = tags;
		this.contatos = contatos;
	}

	public Long getAgendaId() {
		return agendaId;
	}

	public void setAgendaId(Long agendaId) {
		this.agendaId = agendaId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	@JsonIgnore
	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	@JsonIgnore
	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}
}
