package com.joaodjunior.jdjagenda.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjagenda.model.Agenda;
import com.joaodjunior.jdjagenda.repository.AgendaRepository;
import com.joaodjunior.jdjagenda.repository.UsuarioRepository;

@Service
public class AgendaService {
	
	@Autowired
	private AgendaRepository agendaRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public List<Agenda> listarAgendas() {
		List<Agenda> agendas = new ArrayList<Agenda>();
		agendaRepository.findAll().forEach(agendas::add);
		return agendas;
	}
	
	public List<Agenda> listarAgendas(Long usuarioId) {
		List<Agenda> agendas = new ArrayList<Agenda>();
		agendaRepository.findByUsuario(usuarioRepository.findById(usuarioId).get()).forEach(agendas::add);
		return agendas;
	}
	
	public Optional<Agenda> buscarAgenda(Long agendaId) {
		return agendaRepository.findById(agendaId);
	}
	
	public Agenda atualizarAgenda(Agenda agenda) {
		return agendaRepository.save(agenda);
	}
	
	public Agenda inserirAgenda(Agenda agenda) {
		return agendaRepository.save(agenda);
	}
	
	public void removerAgenda(Long agendaId) {
		agendaRepository.deleteById(agendaId);
	}

}
