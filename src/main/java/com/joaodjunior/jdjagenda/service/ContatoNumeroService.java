package com.joaodjunior.jdjagenda.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjagenda.model.Contato;
import com.joaodjunior.jdjagenda.model.ContatoNumero;
import com.joaodjunior.jdjagenda.repository.ContatoNumeroRepository;
import com.joaodjunior.jdjagenda.repository.ContatoRepository;

@Service
public class ContatoNumeroService {

	@Autowired
	private ContatoNumeroRepository contatoNumeroRepository;
	
	@Autowired
	private ContatoRepository contatoRepository;
	
	public List<ContatoNumero> listarNumeros() {
		List<ContatoNumero> numeros = new ArrayList<ContatoNumero>();
		contatoNumeroRepository.findAll().forEach(numeros::add);
		return numeros;
	}
	
	public List<ContatoNumero> listarNumeros(Long contatoId) {
		List<ContatoNumero> numeros = new ArrayList<ContatoNumero>();
		contatoNumeroRepository.findByContato(contatoRepository.findById(contatoId).get()).forEach(numeros::add);
		return numeros;
	}
	
	public Optional<ContatoNumero> buscarNumero(Long numeroId) {
		return contatoNumeroRepository.findById(numeroId);
	}
	
	public Contato buscarNumero(String numero) {
		return contatoNumeroRepository.findByNumero(numero);
	}
	
	public ContatoNumero atualizarNumero(ContatoNumero numero) {
		return contatoNumeroRepository.save(numero);
	}
	
	public ContatoNumero inserirNumero(ContatoNumero numero) {
		return contatoNumeroRepository.save(numero);
	}
	
	public void removerNumero(Long numeroId) {
		contatoNumeroRepository.deleteById(numeroId);
	}
}
