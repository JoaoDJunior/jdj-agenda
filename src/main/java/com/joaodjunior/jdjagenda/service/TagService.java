package com.joaodjunior.jdjagenda.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjagenda.model.Tag;
import com.joaodjunior.jdjagenda.repository.AgendaRepository;
import com.joaodjunior.jdjagenda.repository.TagRepository;

@Service
public class TagService {

	@Autowired
	private TagRepository tagRepository;
	
	@Autowired
	private AgendaRepository agendaRepository;
	
	public List<Tag> listarTag() {
		List<Tag> tags = new ArrayList<Tag>();
		tagRepository.findAll().forEach(tags::add);
		return tags;
	}
	
	public List<Tag> listarTag(Long agendaID) {
		List<Tag> tags = new ArrayList<Tag>();
		tagRepository.findByAgenda(agendaRepository.findById(agendaID).get()).forEach(tags::add);
		return tags;
	}
	
	public Optional<Tag> buscarTag(Long tagId) {
		return tagRepository.findById(tagId);
	}
	
	public Tag atualizarTag(Tag tag) {
		return tagRepository.save(tag);
	}
	
	public Tag inserirTag(Tag tag) {
		return tagRepository.save(tag);
	}
	
	public void removerTag(Long tagId) {
		tagRepository.deleteById(tagId);
	}
	}
