package com.joaodjunior.jdjagenda.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjagenda.model.Contato;
import com.joaodjunior.jdjagenda.repository.AgendaRepository;
import com.joaodjunior.jdjagenda.repository.ContatoRepository;

@Service
public class ContatoService {

	@Autowired
	private ContatoRepository contatoRepository;
	
	@Autowired
	private AgendaRepository agendaRepository;
	
	public List<Contato> listarContatos() {
		List<Contato> contatos = new ArrayList<Contato>();
		contatoRepository.findAll().forEach(contatos::add);
		return contatos;
	}
	
	public List<Contato> listarContatos(Long agendaId) {
		List<Contato> contatos = new ArrayList<Contato>();
		contatoRepository.findByAgenda(agendaRepository.findById(agendaId).get()).forEach(contatos::add);
		return contatos;
	}
	
	public Optional<Contato> buscarContato(Long contatoId) {
		return contatoRepository.findById(contatoId);
	}
	
	public Contato atualizarContato(Contato contato) {
		return contatoRepository.save(contato);
	}
	
	public Contato inserirContato(Contato contato) {
		return contatoRepository.save(contato);
	}
	
	public void removerContato(Long contatoId) {
		contatoRepository.deleteById(contatoId);
	}
}
