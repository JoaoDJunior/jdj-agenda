package com.joaodjunior.jdjagenda.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjagenda.model.Agenda;
import com.joaodjunior.jdjagenda.model.Tag;

public interface TagRepository extends CrudRepository<Tag, Long> {
	
	List<Tag> findByAgenda(Agenda agenda);

}
