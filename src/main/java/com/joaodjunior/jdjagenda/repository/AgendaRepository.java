package com.joaodjunior.jdjagenda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.joaodjunior.jdjagenda.model.Agenda;
import com.joaodjunior.jdjagenda.model.Usuario;

public interface AgendaRepository extends JpaRepository<Agenda, Long> {

	List<Agenda> findByUsuario(Usuario usuario);
}
