package com.joaodjunior.jdjagenda.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjagenda.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	Usuario findUsuarioByEmail(String email);
	
}
