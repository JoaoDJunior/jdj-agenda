package com.joaodjunior.jdjagenda.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjagenda.model.Contato;
import com.joaodjunior.jdjagenda.model.ContatoNumero;

public interface ContatoNumeroRepository extends CrudRepository<ContatoNumero, Long> {

	List<ContatoNumero> findByContato(Contato contato);
	
	Contato findByNumero(String numero);
}
