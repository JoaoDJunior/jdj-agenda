package com.joaodjunior.jdjagenda.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.joaodjunior.jdjagenda.model.Agenda;
import com.joaodjunior.jdjagenda.model.Contato;

public interface ContatoRepository extends JpaRepository<Contato, Long> {
	
	List<Contato> findByAgenda(Agenda agenda);

}
