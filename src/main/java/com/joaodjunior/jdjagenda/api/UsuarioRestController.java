package com.joaodjunior.jdjagenda.api;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjagenda.model.Usuario;
import com.joaodjunior.jdjagenda.service.UsuarioService;
@RestController
@RequestMapping("api/usuario")
public class UsuarioRestController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("listar")
	public List<Usuario> buscarUsuario() {
		return usuarioService.listarUsuarios();
	}
	
	@GetMapping("{email}")
	public Usuario buscarUsuario(@PathParam(value="email") String email) {
		return usuarioService.buscarUsuarioPeloEmail(email);
	}
	
	@PostMapping("inserir")
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		return usuarioService.inserirUsuario(usuario);
	}


}
