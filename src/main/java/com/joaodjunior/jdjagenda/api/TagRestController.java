package com.joaodjunior.jdjagenda.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjagenda.model.Tag;
import com.joaodjunior.jdjagenda.service.TagService;

@RestController
@RequestMapping("api/tag")
public class TagRestController {

	@Autowired
	private TagService tagService;
	
	@GetMapping("listar")
	public List<Tag> listarTags() {
		return tagService.listarTag();
	}
	
	@GetMapping("/listar/agenda/{agendaId}")
	public List<Tag> listarTags(@PathVariable("agendaId") Long agendaId) {
		return tagService.listarTag(agendaId);
	}
	
	@GetMapping("{tagId}")
	public Optional<Tag> buscarTag(@PathVariable("tagId") Long tagId) {
		return tagService.buscarTag(tagId);
	}
	
	@PutMapping("atualizar")
	public Tag atualizarTag(@RequestBody Tag tag) {
		return tagService.atualizarTag(tag);
	}
	
	@PostMapping("inserir")
	private Tag inserirTag(@RequestBody Tag tag) {
		return tagService.inserirTag(tag);
	}
	
	@DeleteMapping("remover/{tagId}")
	private void removerTag(@PathVariable("tagId") Long tagId) {
		tagService.removerTag(tagId);
	}
	
}
