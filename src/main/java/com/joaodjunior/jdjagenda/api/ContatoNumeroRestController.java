package com.joaodjunior.jdjagenda.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjagenda.model.Contato;
import com.joaodjunior.jdjagenda.model.ContatoNumero;
import com.joaodjunior.jdjagenda.service.ContatoNumeroService;

@RestController
@RequestMapping("api/numero")
public class ContatoNumeroRestController {

	@Autowired
	private ContatoNumeroService numeroService;
	
	@GetMapping("listar")
	public List<ContatoNumero> listarNumero() {
		return numeroService.listarNumeros();
	}
	
	@GetMapping("/listar/contato/{contatoId}")
	public List<ContatoNumero> listarNumero(@PathVariable("contatoId") Long contatoId) {
		return numeroService.listarNumeros(contatoId);
	}
	
	@GetMapping("{numeroId}")
	public Optional<ContatoNumero> buscarNumero(@PathVariable("numeroId") Long numeroId) {
		return numeroService.buscarNumero(numeroId);
	}
	
	@GetMapping("get/{numero}")
	public Contato buscarNumero(@PathVariable("numero") String numero) {
		return numeroService.buscarNumero(numero);
	}
	
	@PutMapping("atualizar")
	public ContatoNumero atualizarNumero(@RequestBody ContatoNumero numero) {
		return numeroService.atualizarNumero(numero);
	}
	
	@PostMapping("inserir")
	public ContatoNumero inserirNumero(@RequestBody ContatoNumero numero) {
		return numeroService.inserirNumero(numero);
	}
	
	@DeleteMapping("remover/{numeroId}")
	public void removerNumero(@PathVariable("numeroId") Long numeroId) {
		numeroService.removerNumero(numeroId);
	}
	
}
