package com.joaodjunior.jdjagenda.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjagenda.model.Agenda;
import com.joaodjunior.jdjagenda.service.AgendaService;
@RestController
@RequestMapping("api/agenda")
public class AgendaRestController {
	
	@Autowired
	private AgendaService agendaService;
	
	@GetMapping("listar")
	public List<Agenda> listarAgendas() {
		return agendaService.listarAgendas();
	}
	
	@GetMapping("/listar/usuario/{usuarioId}")
	public List<Agenda> listarAgendas(@PathVariable("usuarioId") Long usuarioId) {
		return agendaService.listarAgendas(usuarioId);
	}
	
	@GetMapping("{agendaId}")
	public Optional<Agenda> buscarAgenda(@PathVariable("agendaId") Long agendaId) {
		return agendaService.buscarAgenda(agendaId);
	}
	
	@PutMapping("atualizar")
	public Agenda atualizarAgenda(@RequestBody Agenda agenda) {
		return agendaService.atualizarAgenda(agenda);
	}
	
	@PostMapping("inserir")
	public Agenda inserirAgenda(@RequestBody Agenda agenda) {
		return agendaService.inserirAgenda(agenda);
	}
	
	@DeleteMapping("remover/{agendaId}")
	public void removerAgenda(@PathVariable("agendaId") Long agendaId) {
		agendaService.removerAgenda(agendaId);
	}

}
