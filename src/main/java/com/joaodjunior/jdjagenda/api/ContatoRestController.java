package com.joaodjunior.jdjagenda.api;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjagenda.model.Contato;
import com.joaodjunior.jdjagenda.service.ContatoService;

@RestController
@RequestMapping("api/contato")
public class ContatoRestController {

	@Autowired
	private ContatoService contatoService;
	
	@GetMapping("listar")
	public List<Contato> listarContatos() {
		return contatoService.listarContatos();
	}
	
	@GetMapping("/listar/agenda/{agendaId}")
	public List<Contato> listarContatos(@PathVariable("agendaId") Long agendaId) {
		return contatoService.listarContatos(agendaId);
	}
	
	@GetMapping("{contatoId}")
	public Optional<Contato> buscarContato(@PathVariable("contatoId")Long contatoId) {
		return contatoService.buscarContato(contatoId);
	}
	
	@PutMapping("atualizar")
	public Contato atualizarContato(@RequestBody Contato contato) {
		return contatoService.atualizarContato(contato);
	}
	
	@PostMapping("inserir")
	public Contato inserirContato(@RequestBody Contato contato) {
		return contatoService.inserirContato(contato);
	}
	
	@DeleteMapping("remover/{contatoId}")
	public void removerContato(@PathVariable("contatoId") Long contatoId) {
		contatoService.removerContato(contatoId);
	}
	
}
